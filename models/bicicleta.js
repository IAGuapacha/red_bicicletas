var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id:' + this.id +" | " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById =  function (aBiciId) {
    var  aBici = Bicicleta.allBicis.find(x => x.id ==aBiciId);
    if (aBici) {
        return aBici
    }else{
        throw new  Error(`No existe una bicicleta con el id ${aBiciId}`);
    }
}

Bicicleta.removeById = function (aBiciId) {
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id ==aBiciId) {
            Bicicleta.allBicis.splice(i,1);
            break;
        }
        
    }
}

var a = new Bicicleta(1,'rojo','urbana', [4.451992, -75.781162]);
var b = new Bicicleta(2,'blanca','urbana', [4.451265, -75.787782]);
var c = new Bicicleta(3,'verde','mtb', [4.452442, -75.791677]);
Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports = Bicicleta;